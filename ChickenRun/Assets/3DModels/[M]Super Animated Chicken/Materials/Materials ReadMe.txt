If you'd like to customize the colors you have two options:

a) light performance:

1) Take texture file "chickenblob_txt" and adjust colors in image processing software like GIMP. Save it in the same folder as chickenblobl_txt with different name.
2) Apply material "TexturedChicken" to all components of the chicken.
3) Change albeido map to newly texture map.

b) heavier performance but quick:
Each part of chicken has different material assigned to it, change the albeido color for your preffered looks.