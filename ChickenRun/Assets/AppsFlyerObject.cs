﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppsFlyerObject : MonoBehaviour
{

    [SerializeField] string IDApple;


    void Start()
    {
        if (IDApple.Equals(""))
        {
            Debug.LogError("IDApple Is empty");
        }

        /* Mandatory - set your AppsFlyer’s Developer key. */

        AppsFlyer.setAppsFlyerKey("8MAzUC3B2BHYVi2uYVHaSd");

        /* For detailed logging */
        /* AppsFlyer.setIsDebug (true); */


        // enviar los datos

        //  AppsFlyer.loadConversionData("AppsFlyerTrackerCallbacks");


#if UNITY_IOS
      /* Mandatory - set your apple app ID
      NOTE: You should enter the number only and not the "ID" prefix */
      AppsFlyer.setAppID (IDApple);
      AppsFlyer.getConversionData();
      AppsFlyer.trackAppLaunch ();
#elif UNITY_ANDROID
        /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
        AppsFlyer.init ("8MAzUC3B2BHYVi2uYVHaSd", "AppsFlyerTrackerCallbacks");
#endif
    }
}