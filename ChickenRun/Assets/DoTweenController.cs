﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Events;

public class DoTweenController : MonoBehaviour
{
    public float _duration;
    public float delay;
    public Vector3 targetScale;
    public Vector3 targetPosition;
    public Vector3 target2Position;
    public Vector3 targetRotation;
    public bool playOnAwake;
    public bool loop;


    Vector3 originalScale;
    Vector3 originalPosition;
    Vector3 originalRotation;

    public enum AnimationType
    {
        scale,
        position,
        rotate,
        rotateAndMoveLocal,
        rotateAndMove,
        birdRevive,
        UIScale,
        UIPositionFadeOut,
        UIPositionFadeIn,
        UIPositionLoop,
        jump
    }
    public AnimationType _animationType;

    public Ease moveEase = Ease.Linear;

    public UnityEvent eventAfterAnimation;


    private void Awake()
    {

        if (transform.parent != null)
        {
            originalScale = transform.localScale;
            originalPosition = transform.localPosition;
            originalRotation = transform.rotation.eulerAngles;

        }
        else
        {

            originalScale = transform.localScale;
            originalPosition = transform.position;
            originalRotation = transform.rotation.eulerAngles;
        }
        

        if (playOnAwake)
        {
            Invoke("CheckAnimation",delay);
            StartCoroutine(StartEvent());
        }
       
    }

    private void OnEnable()
    {

        //if (playOnAwake)
        //{
        //    transform.localScale = originalScale;
        //    transform.localPosition = originalPosition;
        //    Invoke("CheckAnimation", delay);

        //}
    }

    public void CheckAnimation()
    {
        //Reset 
        ResetScale();

        var _sequ = DOTween.Sequence();

        switch (_animationType)
        {
            case AnimationType.position:

                transform.localPosition = originalPosition;
                transform.localScale = originalScale;
                transform.DOScale(targetScale, _duration).SetEase(moveEase);
                transform.DOLocalMove(targetPosition,_duration,false).SetEase(moveEase);
                break;

            case AnimationType.scale:

                transform.localScale = originalScale;
                transform.DOScale(targetScale, _duration).SetEase(moveEase);
                break;

            case AnimationType.rotate:

                transform.DORotate(targetRotation, _duration, RotateMode.Fast).SetEase(moveEase).SetLoops(2, LoopType.Yoyo);

                break;
            case AnimationType.rotateAndMoveLocal:

                _sequ.Append(transform.DOLocalMove(targetPosition, _duration/2, false).SetEase(moveEase));
                _sequ.Insert((delay+_duration)*0.6f,transform.DORotate(targetRotation, _duration, RotateMode.Fast).SetEase(moveEase));
                _sequ.Append(transform.DOLocalMove(target2Position, _duration/3, false).SetEase(moveEase));

                break;

            case AnimationType.UIScale:

                transform.DOScale(targetScale, _duration).SetEase(moveEase);
                break;

            case AnimationType.jump:

                transform.DOJump(targetPosition,3,1,_duration,false).SetEase(moveEase);
                break;

            case AnimationType.UIPositionFadeOut:


                GetComponent<RectTransform>().DOAnchorPos(targetPosition,_duration,false).SetEase(moveEase).SetLoops(5,LoopType.Yoyo);
                _sequ.Insert((delay+_duration)*1.2f,GetComponent<Image>().DOFade(0,0.5f));


                break;

            case AnimationType.UIPositionFadeIn:

                GetComponent<RectTransform>().DOAnchorPos(targetPosition, _duration, false).SetEase(moveEase).SetLoops(5, LoopType.Yoyo);
                _sequ.Insert(delay, GetComponent<Image>().DOFade(1, 0.5f));


                break;

            case AnimationType.UIPositionLoop:


                GetComponent<RectTransform>().DOAnchorPos(targetPosition, _duration, false).SetEase(moveEase).SetLoops(60, LoopType.Yoyo);
           

                break;
        }

        if (loop)
        {
            CancelInvoke();
            Invoke("CheckAnimation",(delay + _duration));

        }
    }

    IEnumerator StartEvent()
    {
        yield return new WaitForSeconds((delay + _duration));
        if (eventAfterAnimation != null)
        {
            eventAfterAnimation.Invoke();
        }

    }

    void SetRotation(Vector3 newRotation)
    {
        targetRotation = newRotation;
    }
    void SetOriginalPosition(Vector3 origin)
    {
        originalPosition = origin;
    }

    void SetTargetPosition(Vector3 new2Targe)
    {
        targetPosition = new2Targe;

    }

    void SetTarget2Position(Vector3 new2Targe)
    {

        target2Position = new2Targe;
    }

    void ResetScale()
    {
        transform.localScale = originalScale;
    }
}
