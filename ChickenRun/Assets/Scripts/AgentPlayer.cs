﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using MyBox;

[RequireComponent(typeof(PlayerID))]
public class AgentPlayer : MonoBehaviour, IRunner, IEmotion
{
    NavMeshAgent agentComponent;
    float bufferSpeed;
    float speedBonus;
    float timeShot;

    PlayerID idPlayer;
    //[HideInInspector]
    public Transform target;
    [SerializeField] GameObject container;
    GameObject[] balls;
    float minDistGifts = 4;

    bool IMoving = true;
   // public bool increseSpeedPerBox;
    
    Transform currentPos;
    Transform previousPos;

    [HideInInspector]
    public PlayerID[] playerToHunt;

    public GameObject visualMesh;


    bool iHaveTarget;
    [Tooltip("Change the behaviour")]
    public bool useRandomBehaviour;

    [SerializeField] GameObject dashParticle;


    [ConditionalField(nameof(useRandomBehaviour))] [Tooltip("time intervale to change the behaviour")] [Range(1, 30)] public float timeInvokeBehaviour = 1;

    public enum PlayerBehaviour
    {
        Coward,
        Hunter,
        Holder,
        HolderRandom,
        PlayerTrouble
    }
    [Tooltip("- COWARD: the agent search 1 gift and throw in deployment zone \n"
        + "- HUNTER: Seek the player with highest score, if he is the highest would go deployment zone \n"
        + "- HOLDER: Seek the closer gifts in floor, when it have 7 gifts would go deployment zone \n"
        + "- HOLDERRANDOM: Seek a random gift, when it have 7 gifts would go deployment zone \n"
        + "- PLAYERTROUBLE: it follow the PLAYER, if it have more gift return to deployment zone ")]
    public PlayerBehaviour myBehaviour;


    public System.Action<Emotion> OnEmotion { get; set; }

    PlayerBehaviour bufferBehaviour;
    bool changedBehaviour;

    void Start()
    {
        playerToHunt = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().players;

        idPlayer = GetComponent<PlayerID>();

        if (container == null)
        {
            Debug.LogError("ASSIGN A CONTAINER IN INSPECTOR");
        }


        idPlayer.gifts.Add(gameObject);
        agentComponent = GetComponent<NavMeshAgent>();
        bufferSpeed = agentComponent.speed;
        speedBonus = bufferSpeed;

        if (useRandomBehaviour)
        {
            ChangeBehaviour();
        }

        Stop();

    }

    bool CheckObjectInGifts(GameObject obj)
    {
        bool result = false;

        foreach (GameObject gify in idPlayer.gifts)
        {
            if (ReferenceEquals(obj, gify))
            {
                result = true;
                break;
            }

        }

        return result;
    }


    void Update()
    {
        //antes de huevos
        //agentComponent.speed = bufferSpeed + ((increseSpeedPerBox) ? 0.2f : -0.2f * (idPlayer.gifts.Count - 2)) + speedBonus;


        if (agentComponent.isStopped)
        {
            return;
        }

        if (!idPlayer.soonIDie)
        {
            visualMesh.transform.localRotation = Quaternion.Euler(0, 180, 0);
        }


        agentComponent.speed = speedBonus;

        if (idPlayer.gifts.Count > 2)
        {
            idPlayer.textNameOver.gameObject.SetActive(true);

        }

        if (IMoving)
        {
            for (int i = 1; i < idPlayer.gifts.Count; i++)
            {
                currentPos = idPlayer.gifts[i].transform;
                previousPos = idPlayer.gifts[i - 1].transform;

                float dist = Vector3.Distance(previousPos.position, currentPos.position);
                Vector3 newPos = previousPos.position;
                newPos.y = transform.position.y;

                float t = Time.deltaTime * dist / minDistGifts * 6;

                idPlayer.gifts[i].transform.position = Vector3.Slerp(currentPos.position, newPos, t);

            }

        }


        switch (myBehaviour)
        {
            case PlayerBehaviour.Coward:
                CowardBehaviour();

                break;

            case PlayerBehaviour.Holder:
                HolderBehaviour();

                break;

            case PlayerBehaviour.Hunter:
                HunterBehaviour();

                break;

            case PlayerBehaviour.PlayerTrouble:
                HunterBehaviour("Player");
                break;

            case PlayerBehaviour.HolderRandom:
                HolderRandomBehaviour();

                break;

        }

        if (target != null)
        {
            
            if (idPlayer.gifts.Contains(target.gameObject) || agentComponent.velocity.magnitude < 0.02f)
            {

                bufferBehaviour = myBehaviour;
                changedBehaviour = false;
                timeInvokeBehaviour = 7;
                CancelInvoke();
                ChangeBehaviour();
            }

            agentComponent.SetDestination(target.position);

        }

    }

    

    void ChangeBehaviour()
    {

        if (!useRandomBehaviour && changedBehaviour)
        {
            myBehaviour = bufferBehaviour;
            return;

        }

        int rand = Random.Range(0, 5);

        switch (rand)
        {
            case 0:

                myBehaviour = PlayerBehaviour.Coward;
                break;

            case 1:
                myBehaviour = PlayerBehaviour.Holder;
                break;

            case 2:
                myBehaviour = PlayerBehaviour.Hunter;
                break;

            case 3:
              //  myBehaviour = PlayerBehaviour.Hunter;

                 myBehaviour = PlayerBehaviour.PlayerTrouble;
                break;

            case 4:
                myBehaviour = PlayerBehaviour.HolderRandom;
                break;

        }

        changedBehaviour = true;


        Invoke("ChangeBehaviour", timeInvokeBehaviour);

    }

    void HunterBehaviour()
    {
        for (int i = 0; i < playerToHunt.Length - 1; i++)
        {
            for (int j = 0; j < playerToHunt.Length - 1; j++)
            {
                if (playerToHunt[j].gifts.Count < playerToHunt[j + 1].gifts.Count)
                {
                     PlayerID buffer = playerToHunt[j + 1];
                    playerToHunt[j + 1] = playerToHunt[j];
                    playerToHunt[j] = buffer;
                }
            }
        }

        if (playerToHunt[0].gifts.Count > 1)
        {
            if (ReferenceEquals(playerToHunt[0].gameObject, gameObject))
            {
                target = container.transform;

                if (Vector3.Distance(transform.position, container.transform.position) < 9)
                {

                    timeShot += Time.deltaTime;
                   //  print(timeShot);

                    if (timeShot > 0.1f)
                    {
                       // print("ads "+name);

                        ShotGift();
                        timeShot = 0;
                    }
                }

            }
            else
            {

                target = playerToHunt[0].gifts[1].transform;

            }

        }
        else
        {
            FindBall();
        }

        if (target != null)
        {
            agentComponent.SetDestination(target.position);

        }

    }

    void HunterBehaviour(string tag)
    {
        GameObject playerMain = GameObject.FindGameObjectWithTag(tag);

        if (playerMain != null && playerMain.GetComponent<PlayerID>().gifts.Count >= idPlayer.gifts.Count)
        {
                target = playerMain.transform;
        }
        else
        {
            if (Vector3.Distance(transform.position, container.transform.position) < 9)
            {
                timeShot += Time.deltaTime;
                if (timeShot > 0.1f && idPlayer.gifts.Count > 1)
                {
                    ShotGift();
                    timeShot = 0;

                }
                else
                {
                    FindRandomBall();
                }
            }
            else
            {
                target = container.transform;

            }
        }

        if (target != null)
        {
            agentComponent.SetDestination(target.position);

        }

    }

    void HolderBehaviour()
    {
        if (Vector3.Distance(transform.position, container.transform.position) < 9 )
        {
            if (idPlayer.gifts.Count < 2)
            {
                //ReferenceEquals(target, container) || target == null || !target.gameObject.activeInHierarchy || 
                if (target == null || !target.CompareTag("Ball") || !target.gameObject.activeInHierarchy || ReferenceEquals(target, container))
                {
                  //  FindRandomBall();
                    FindBall();

                    iHaveTarget = true;
                }

            }
            else
            {
                timeShot += Time.deltaTime;
                if (timeShot > 0.1f)
                {
                    ShotGift();
                    timeShot = 0;
                }

                if (idPlayer.gifts.Count < 2)
                {
                    iHaveTarget = false;
                }
            }
        }
        else
        {
            if (idPlayer.gifts.Count < 8 && !iHaveTarget)
            {
                // 
                if (target == null || !target.CompareTag("Ball") || !target.gameObject.activeInHierarchy || ReferenceEquals(target, container))
                {
                   // FindRandomBall();
                    FindBall();
                }
            }
            else
            {
                iHaveTarget = false;

                target = container.transform;

                if (Vector3.Distance(transform.position, target.position) < 9)
                {
                    timeShot += Time.deltaTime;
                    if (timeShot > 0.1f)
                    {
                        ShotGift();
                        timeShot = 0;
                    }

                    if (idPlayer.gifts.Count < 2)
                    {
                        iHaveTarget = false;
                    }
                }

            }
        }

        if (target != null)
        {
            agentComponent.SetDestination(target.position);

        }
    }

    void HolderRandomBehaviour()
    {
        if (Vector3.Distance(transform.position, container.transform.position) < 9)
        {
            if (idPlayer.gifts.Count < 2)
            {
                //ReferenceEquals(target, container) || target == null || !target.gameObject.activeInHierarchy || 
                if (target == null || !target.CompareTag("Ball") || !target.gameObject.activeInHierarchy || ReferenceEquals(target, container))
                {
                    FindRandomBall();

                    iHaveTarget = true;
                }
            }
            else
            {
                timeShot += Time.deltaTime;
                if (timeShot > 0.1f)
                {
                    ShotGift();
                    timeShot = 0;
                }

                if (idPlayer.gifts.Count < 2)
                {
                    iHaveTarget = false;
                }
            }
        }
        else
        {
            if (idPlayer.gifts.Count < 8 && !iHaveTarget)
            {
                // 
                if (target == null || !target.CompareTag("Ball") || !target.gameObject.activeInHierarchy || ReferenceEquals(target, container))
                {
                    FindRandomBall();
                   
                }
            }
            else
            {
                iHaveTarget = false;

                target = container.transform;

                if (Vector3.Distance(transform.position, target.position) < 9)
                {

                    timeShot += Time.deltaTime;
                    if (timeShot > 0.1f)
                    {
                        ShotGift();
                        timeShot = 0;
                    }

                    if (idPlayer.gifts.Count < 2)
                    {
                        iHaveTarget = false;
                    }
                }

            }
        }

        if (target != null)
        {
            agentComponent.SetDestination(target.position);

        }
    }

    //TODO
    void KillerBehaviour()
    {
            
    }

    void CowardBehaviour()
    {
        if (idPlayer.gifts.Count > 1)
        {
            target = container.transform;
            if (Vector3.Distance(transform.position, target.position) < 9)
            {

                timeShot += Time.deltaTime;
                if (timeShot > 0.1f)
                {
                    ShotGift();
                    timeShot = 0;
                }
            }
        }
        else
        {
            FindBall();
        }

        agentComponent.SetDestination(target.position);

    }

    public IEnumerator ImproveVelocity()
    {
        //speedBonus = bufferSpeed;
        dashParticle.SetActive(true);
        speedBonus = bufferSpeed * 1.8f;

        yield return new WaitForSeconds(2f);
        dashParticle.SetActive(false);

        LessVelocity();
    }

    public void LessVelocity()
    {
        //ANtes de huevos
        //speedBonus = 0;
        speedBonus = bufferSpeed;
    }

    void ShotGift()
    {
        Vector3 direction = container.transform.position - transform.position;
        OnEmotion?.Invoke(Emotion.Derision);

        //print("Shot");
        idPlayer.gifts[1].transform.position = transform.position + transform.localScale+transform.up * 1.2f + transform.forward * 1.2f;
        //idPlayer.gifts[1].GetComponent<Rigidbody>().AddForce(transform.forward * 20, ForceMode.Impulse);
        idPlayer.gifts[1].GetComponent<Rigidbody>().AddForce(direction * 4, ForceMode.Impulse);
        idPlayer.gifts[1].GetComponent<Rigidbody>().useGravity = true;
        idPlayer.gifts[1].GetComponent<Collider>().isTrigger = false;
        idPlayer.gifts[1].GetComponent<Ball>().collected = false;
        idPlayer.gifts[1].GetComponent<Ball>().shoted = true;
        idPlayer.gifts[1].GetComponent<Ball>().SetOwner(null);
        idPlayer.gifts.RemoveAt(1);
        
    }



    void RestoreCollider()
    {
        GetComponent<Collider>().enabled = true;

    }

    private void OnEnabled()
    {
        // GetComponent<Rigidbody>().velocity = Vector3.zero;
        agentComponent.enabled = true;
    }

    void FindRandomBall()
    {
        balls = GameObject.FindGameObjectsWithTag("Ball");
        
        if (balls.Length > 0)
        {
            int ran = Random.Range(0, balls.Length);

            if (!CheckObjectInGifts(balls[ran]))
            {
                if (balls[ran].GetComponent<Ball>().weight > idPlayer.gifts.Count)
                {
                    return;
                }
                target = balls[ran].transform;
            }
        }
        else
        {
            target = container.transform;
        }

    }

    void FindBall()
    {
        balls = GameObject.FindGameObjectsWithTag("Ball");

        if (balls.Length <= 0) { return; }
        target = balls[0].transform;
        float dist = Vector3.Distance(target.position, transform.position);

        foreach (GameObject _giftsBox in balls)
        {
            if (Vector3.Distance(_giftsBox.transform.position, transform.position) < dist && !_giftsBox.GetComponent<Ball>().collected)
            {
                target = _giftsBox.transform;
                dist = Vector3.Distance(_giftsBox.transform.position, transform.position);
            }
        }

        if (target.GetComponent<Ball>().weight > idPlayer.gifts.Count)
        {
            target = balls[6].transform;

        }
    }

    void Stop()
    {
        agentComponent.isStopped = true;
        GetComponent<Collider>().enabled = false;
    }

    void DeadAnimation()
    {
        visualMesh.SendMessage("CheckAnimation");
        visualMesh.SendMessage("SetTarget2Position", new Vector3(0, -1, 0));
        visualMesh.SendMessage("SetRotation", Vector3.zero);
    }

    void Continue()
    {
        agentComponent.isStopped = false;
        GetComponent<Collider>().enabled = true;
    }

    void ReviveAnimation()
    {
        visualMesh.SendMessage("CheckAnimation");
        visualMesh.SendMessage("SetTarget2Position", new Vector3(0, 1, 0));
        visualMesh.SendMessage("SetRotation", new Vector3(0, 0, 180));


    }
}
