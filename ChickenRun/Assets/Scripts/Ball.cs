﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public GameObject owner;
    [HideInInspector]
    public bool collected;
    public bool inMainScene;
    [HideInInspector]
    public bool shoted;
    public int weight=0;
    Vector3 startPosition;
    Renderer render;
    [SerializeField] GameObject particles;
    [SerializeField] GameObject failParticles;
    Rigidbody rb;

    private void Awake()
    {
        startPosition = transform.position;
        render = GetComponent<Renderer>();
        rb = GetComponent<Rigidbody>();


    }

    private void OnDisable()
    {
        if (owner != null)
        {
            owner.SendMessage("TakeAwayGift", gameObject);
        }

        if (inMainScene)
        {
            Invoke("ActiveByTime", 3);

        }
    }

    void ActiveByTime()
    {
        gameObject.SetActive(true);
        transform.position = startPosition;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
    

    //public void MoveBall(Vector3 dir)
    //{
    //    GetComponent<Rigidbody>().AddForce(new Vector3(dir.x, 0, dir.y).normalized * 0.2f * dir.magnitude,
    //        ForceMode.Impulse);

    //}


    public void SetOwner(GameObject newOwner)
    {
        if (newOwner == null)
        {
             owner = null;

            if (GetComponent<UnityEngine.AI.NavMeshObstacle>())
            {
                GetComponent<UnityEngine.AI.NavMeshObstacle>().enabled = true;
            }
        }
        else
        {
            if (newOwner.GetComponent<PlayerID>().gifts.Count >= weight)
            {
                collected = true;
                if (owner != null && !ReferenceEquals(newOwner, owner))
                {
                    owner.SendMessage("TakeAwayGifts", gameObject);
                }
                owner = newOwner;
            }
        }
        if (owner != null)
        {
            
            render.materials[0].SetColor("_BaseColor", owner.GetComponent<Renderer>().materials[0].GetColor("_BaseColor"));

            if (GetComponent<UnityEngine.AI.NavMeshObstacle>())
            {
                GetComponent<UnityEngine.AI.NavMeshObstacle>().enabled = false;
            }
        }
        else
        {
            render.materials[0].SetColor("_BaseColor", Color.white);
            Explotion();
        }

    }

    void ActiveParticles()
    {
        if (particles != null)
        {
            particles.SetActive(false);
            particles.SetActive(true);
            particles.transform.position = transform.position + Vector3.up;
            particles.transform.SetParent(null);
        }

    }

    void Explotion()
    {
        rb.AddForce(Vector3.up * 9, ForceMode.Impulse);

    }
}
