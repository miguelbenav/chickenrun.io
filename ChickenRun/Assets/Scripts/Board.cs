﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Board : MonoBehaviour
{

    public ScoringSystem[] _texts;

    [HideInInspector]
    protected ScoringSystem[] sortedTexts;


    private void Awake()
    {
        sortedTexts = _texts;
    }

    public ScoringSystem GetPlayerAtPosition(int boardPosition)
    {
        return sortedTexts[boardPosition];
    }

    public void SortScores()
    {
        for (int i = 0; i < sortedTexts.Length - 1; i++)
        {
            for (int j = 0; j < sortedTexts.Length - 1; j++)
            {
 
                if (sortedTexts[j].score < sortedTexts[j + 1].score)
                {
                  //  print(j+","+(j+1));

                    ScoringSystem buffer = sortedTexts[j + 1];
                    sortedTexts[j + 1] = sortedTexts[j];
                    sortedTexts[j] = buffer;
                }
            }
        }

        for (int h = 0; h < sortedTexts.Length; h++)
        {
            sortedTexts[h].transform.parent.transform.SetSiblingIndex(h);

        }

    }
}
