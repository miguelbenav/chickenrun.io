﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] Vector3 offset;
    Vector3 beginOffset;
    [SerializeField]GameObject target;
    [SerializeField] GameObject mediumPoint;
    

    Vector3 viewPoint;
    float timeMove;

    void Awake()
    {
        mediumPoint.transform.SetParent(null);

        beginOffset = offset;
    }
    void LateUpdate()
    {
        transform.position = Vector3.Slerp(transform.position,target.transform.position+offset,Time.deltaTime);

        transform.LookAt(target.transform.position);
    }

    public void SetTarget(GameObject newTarget)
    {
        target = newTarget;
    }
    public void ActiveWinningCamera()
    {
        target = mediumPoint;
    }

    public void Zoom(Transform zoomPoint)
    {
     //  mediumPoint.transform.position = zoomPoint.position - target.transform.position;

      float distLastGift = Vector3.Distance(target.transform.position, zoomPoint.position);


        offset.y= beginOffset.y + distLastGift;
    }
}
