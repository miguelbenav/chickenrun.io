﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRay : MonoBehaviour
{

    Camera _camera;

    void Start()
    {
        _camera = GetComponent<Camera>();
    }

    bool onObject;
    Collider bufferCollider;

    // Update is called once per frame
    void Update()
    {

        Ray ray = _camera.ScreenPointToRay(new Vector3(Screen.width/2 , Screen.height / 2, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 90f))
        {
            if (hit.collider.CompareTag("TransparentObject"))
            {
                onObject = true;
                hit.collider.SendMessage("ApplyTransparency");
                bufferCollider = hit.collider;

            }
            else
            {
                if (onObject)
                {
                    bufferCollider.SendMessage("ResetMaterial");
                    onObject = false;

                }
            }
        }
        
    }
}
