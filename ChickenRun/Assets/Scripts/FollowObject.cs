﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour
{
    [SerializeField] PlayerID _player;
    [SerializeField] private Vector3 offsetPos;

    private void OnEnable()
    {
        transform.SetParent(null);
   //     _player.OnPlayerMove += SetPosition;
    //    _player.OnPlayerDead += DisableHeart;
    }

    private void OnDisable()
    {
    //    _player.OnPlayerMove -= SetPosition;
    //    _player.OnPlayerDead -= DisableHeart;
    }

    void SetPosition() 
   {
        transform.position = _player.transform.position + offsetPos;
   }

    void DisableHeart() 
    {
        for(int i = 0; i<3; i++) 
        {
            if (transform.GetChild(i).gameObject.activeInHierarchy) 
            {
                transform.GetChild(i).gameObject.SetActive(false);
                break;

            }
        }
    }
}
