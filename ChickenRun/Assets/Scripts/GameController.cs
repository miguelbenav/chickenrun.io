﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.AI;

[RequireComponent(typeof(Board))]
public class GameController : MonoBehaviour
{
    [SerializeField] bool MainScene = false;
    public float countdown = 30;
    float timeDead = 0;

    [SerializeField] TextMeshProUGUI winerText;
    [SerializeField] TextMeshProUGUI textTime;
    string bufTimeTxt;
    [SerializeField] TextMeshProUGUI textCountDown;
    [SerializeField] TextMeshProUGUI deadTimeText;
    string bufDeadTxt;


    [Header("~~~~~~ Podium ~~~~~")]
    [SerializeField] Transform[] podium;
    [SerializeField] Camera winningCamera;
    [SerializeField] Canvas gameplayCanvas;
    [SerializeField] Canvas winingCanvas;
    bool useAnimatorController;

    Board leaderBoard;

    bool gameOver;

    [Header("~~~~~~~~ Players ~~~~~~~~~")]
    public PlayerID[] players;
    [SerializeField] private GameObject canvasNamePlayer;
    bool gameStarted;



    private void Awake()
    {
        bufTimeTxt = textTime.text;
        bufDeadTxt = deadTimeText.text;

        leaderBoard = GetComponent<Board>();
        Time.timeScale = 1;
    }

    void LosePlayer()
    {
        timeDead = 4.1f;
    }

    public void SetNamesBoard()
    {

        for (int i = 0; i < leaderBoard._texts.Length; i++)
        {

            players[i].UpdateName();
            leaderBoard._texts[i].SetNamePlayerScoringSystem(players[i].playerName);
        }

        Invoke("StartPlayers", 0.1f);
    }

    private void Update()
    {

        if (!gameStarted) { return; }


        if (countdown < 0)
        {
            if (!gameOver)
            {
                CheckWinner();
            }
            gameOver = true;
        }
        else
        {
            if (!MainScene)
            {
                textTime.text = bufTimeTxt+GetTime(countdown,true);

            }

            if (countdown < 10)
            {
                textCountDown.text = GetTime(countdown, false);
            }
            
        }

        if (!MainScene)
        {
            if (timeDead <= 0)
            {
                countdown -= Time.deltaTime;
                deadTimeText.gameObject.SetActive(false);
            }
            else
            {
                deadTimeText.gameObject.SetActive(true);
                timeDead -= Time.deltaTime;
                deadTimeText.text = bufDeadTxt+GetTime(timeDead,false);

            }

        }
    }

    string GetTime(float time,bool withZeros)
    {
        int min = (int)time / 60;
        int sec = (int)time - (min * 60);

        string StrMin = "" + min;
        if (min < 10)
        {
            StrMin = "0" + min;
        }

        string StrSec = "" + sec;
        if (sec < 10)
        {
            StrSec = "0" + sec;
        }

        if (withZeros)
        {
            return string.Concat(StrMin, ":", StrSec);
        }
        else
        {
            return ""+sec;
        }
    }

    public void AddGift()
    {
      leaderBoard.SortScores();
    }

    public void StartPlayers()
    {
        gameStarted = true;
        foreach (PlayerID player in players)
        {
            player.SendMessage("Continue");
        }
    }

    void CheckWinner()
    {
        winerText.gameObject.SetActive(true);
        winerText.text = "" + leaderBoard.GetPlayerAtPosition(0)._namePlayer+" Wins";
        Camera.main.enabled = false;
        winningCamera.enabled = true;
        winingCanvas.enabled = true;

        gameplayCanvas.enabled = false;

        int count = 0;

        players[0].SendMessage("Stop");
        players[1].SendMessage("Stop");
        players[2].SendMessage("Stop");
        players[3].SendMessage("Stop");


        while (count < 4)
        {
            for (int i = 0; i < players.Length; i++)
            {
                if (leaderBoard.GetPlayerAtPosition(count)._namePlayer.Equals(players[i].playerName))
                {
                    GameObject g = Instantiate(players[i].GetVisualPlayerObject(), podium[count].position, podium[count].rotation);
                    g.transform.position = podium[count].position;

                    GameObject canvasPlay = Instantiate(canvasNamePlayer,null);
                    canvasPlay.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = players[i].playerName;
                    canvasPlay.transform.position = g.transform.position + new Vector3(0, 2f, -3);
                    canvasPlay.transform.rotation = Quaternion.Euler(0,180,0);

                    // the first Place
                    if (count == 0)
                    {
                        if (!useAnimatorController)
                        {
                            // with doTweenController;
                            g.SendMessage("SetRotation",Vector3.zero);
                            g.SendMessage("SetOriginalPosition", podium[count].position);
                            g.SendMessage("SetTargetPosition", podium[count].position + (Vector3.up * 2));
                            g.SendMessage("SetTarget2Position", podium[count].position);
                            g.SendMessage("CheckAnimation");

                        }
                        else
                        {
                            g.GetComponent<Animator>().SetBool("Win", true);
                        }

                    }
                    else
                    {
                        if (!useAnimatorController)
                        {
                            // with doTweenController;
                            g.SendMessage("SetRotation", new Vector3(0, 0, 180));
                            g.SendMessage("SetOriginalPosition", podium[count].position);
                            g.SendMessage("SetTargetPosition", podium[count].position + (Vector3.up * 2));
                            g.SendMessage("SetTarget2Position", podium[count].position + Vector3.up);
                            g.SendMessage("CheckAnimation");


                        }
                        else
                        {
                            g.GetComponent<Animator>().SetBool("Lose", true);
                        }

                    }


                    players[i].enabled = false;

                    if (players[i].GetComponent<PlayerController>())
                    {
                        players[i].GetComponent<PlayerController>().enabled = false;
                    }
                }
            }
            count++;
        }

    }
}
