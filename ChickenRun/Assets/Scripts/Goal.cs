﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Goal : MonoBehaviour
{
    public UnityEvent eventGoal;
    [SerializeField] ScoringSystem scoreTo;


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            Ball theBall = other.GetComponent<Ball>();
            theBall.SendMessage("ActiveParticles");
            scoreTo.AddScore(theBall.weight);
            eventGoal?.Invoke();
            other.gameObject.SetActive(false);
            theBall.SetOwner(null);
        }
    }
}
