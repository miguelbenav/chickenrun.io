﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{


    public float SwipeMagnitudeMove = 10;
    [Tooltip("Pass Muste be higher than Swipe Move")]
    public float SwipeMagnitudePass = 20;
    public event System.Action<Vector3>OnDragged;
    public event System.Action<Vector3> OnDraggedEnd;

    public static InputManager Instance { get; private set; }

    [SerializeField] Vector3[] previousPointerPos;
  //  [SerializeField] Image _imagDes;
  //  [SerializeField] Image _imagSwipe1;
  //  [SerializeField] Image _imagSwipe2;
   // [SerializeField] Image _imagMagnitu;

    Vector3 startPosition;
    Vector3 currentPosition;

    int indexPos;

    private void Awake()
    {
        Instance = this;
        previousPointerPos = new Vector3[7];

        Application.targetFrameRate = 30;
    }

    private void Update()
    {
    #if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0))
        {
            startPosition = Input.mousePosition;
        //    _imagDes.GetComponent<RectTransform>().position = startPosition;


        }

        

        if (Input.GetMouseButton(0) && (Input.mousePosition - startPosition).magnitude > SwipeMagnitudeMove)
        {
            currentPosition = Input.mousePosition;
            OnDragged?.Invoke(GetDirection(currentPosition - startPosition));

        }

        previousPointerPos[indexPos] = Input.mousePosition;
       // int firstIndex = (indexPos == 6) ? 0 : indexPos++;
      //  _imagSwipe2.GetComponent<RectTransform>().position = previousPointerPos[firstIndex];

        if (Input.GetMouseButtonUp(0))
        {
            OnDraggedEnd?.Invoke(GetDraggEnd());
        }


#else
        
         if (Input.touchCount == 0) return;

        currentPosition = Input.GetTouch(0).position;

        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startPosition = Input.GetTouch(0).position;
        }
        if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {


            if ((currentPosition - startPosition).magnitude > SwipeMagnitudeMove)
            {
                OnDragged?.Invoke(GetDirection(currentPosition - startPosition));
                previousPointerPos[indexPos] = Input.GetTouch(0).position;



            }

        }
        if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            OnDraggedEnd?.Invoke(GetDraggEnd());

        }
      



#endif

        indexPos++;
        if (indexPos > 6)
        {
            indexPos = 0;

        }
    }

    public Vector3 GetDirection(Vector3 direct)
    {
        Vector3 dir = direct;
        
        return dir;
    }

    public Vector3 GetDraggEnd()
    {
        int firstIndex = (indexPos == 6) ? 0 : indexPos++;
    //    _imagSwipe1.GetComponent<RectTransform>().position = previousPointerPos[indexPos];

        Vector3 force = Input.mousePosition - previousPointerPos[indexPos];
        //  _imagMagnitu.GetComponent<RectTransform>().position = force.normalized;

//        print(force.magnitude);
        if (force.magnitude > SwipeMagnitudePass)
        {
            return force;

        }
        else
        {
            return Vector3.zero;
        }

        
    }
}
