﻿using UnityEngine;
using System.Collections;
using System;

public interface IPlayer
{
    void TakeAwayGifts(GameObject gift);
    void TakeAwayGift(GameObject gift);

    // void GetGift(GameObject indexGift);

}

public interface IRunner
{

    IEnumerator ImproveVelocity();

}

public interface IEmotion
{
    Action<Emotion> OnEmotion { get; set;}

}

public enum Emotion
{

    Happy,
    Sad,
    Angry,
    Derision

}