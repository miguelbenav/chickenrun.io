﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MyBox;

using TMPro;
public class LevelManager : MonoBehaviour
{
    //[SerializeField] TMP_InputField inputField;
    [Tooltip("Check it if stay in mani scene")]
    [SerializeField] bool inMainScene;

    [ConditionalField(nameof(inMainScene),true)] [SerializeField] TMP_InputField inputField;



    private void Awake()
    {
        PlayerPrefs.DeleteKey("Name");
        PlayerPrefs.SetInt("Open", 1);

        if (PlayerPrefs.GetString("Name").Equals(""))
        {
            PlayerPrefs.SetString("Name", "Player");
        }


        if (inputField != null)
        {
            inputField.text = PlayerPrefs.GetString("Name");

        }


        if (inMainScene)
        {
            StartCoroutine(LoadAfterTime());
        }
    }

    public IEnumerator LoadAfterTime()
    {
        yield return new WaitForSeconds(1.2f);
        LoadLevel(1);
    }

    public void Replay()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);

    }

    public void LoadLevel(int levelIndex)
    {
        SceneManager.LoadSceneAsync(levelIndex);

    }

    public void SetName()
    {
        PlayerPrefs.SetString("Name", inputField.text);
        PlayerPrefs.GetString("Name");

        if (GetComponent<GameController>())
        {
            GetComponent<GameController>().SetNamesBoard();
        }
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("Open",0);
    }
}
