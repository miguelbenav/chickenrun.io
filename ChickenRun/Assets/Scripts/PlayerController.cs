﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(PlayerID))]
public class PlayerController : MonoBehaviour, IRunner,IEmotion
{
    public float speed=8;
    float bufferSpeed;
    float speedBonus;
    float minDistGifts = 4;
    float timeShot;

    bool IMoving;
    bool inDeploymentZone;

    Transform currentPos;
    Transform previousPos;

    PlayerID idPlayer;

    [SerializeField] Transform container;
    //public bool increseSpeedPerBox;

    [SerializeField] bool invertMovement;
    [SerializeField] Vector2 verticalLimits;
    [SerializeField] Vector2 horizontallLimits;

    public System.Action<Emotion> OnEmotion { get; set; }

    int indexGifts;

    NavMeshAgent _agent;
    [SerializeField] GameObject dashParticle;
    public GameObject visualMesh;


    void Awake()
    {
        if (GetComponent<NavMeshAgent>())
        {
            _agent = GetComponent<NavMeshAgent>();

            _agent.speed = speed;
            bufferSpeed = _agent.speed;
            speedBonus = speed;

            _agent.updateRotation = false;

        }
        else
        {
            bufferSpeed = speed;
            speedBonus = speed;
        }
        idPlayer = GetComponent<PlayerID>();

        
        idPlayer.gifts.Add(gameObject);
        Stop();
        
    }
   

    void Start()
    {
        if (InputManager.Instance != null)
        {
            InputManager.Instance.OnDragged += MovePlayer;

        }

        if(targei!= null)
        {
            targei.SetParent(null);

        }
    }

    void OnEnable()
    {
        if (InputManager.Instance != null)
        {
            InputManager.Instance.OnDragged += MovePlayer;
        }
    }


    public void OnDisable()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;

        if (InputManager.Instance != null)
        {
            InputManager.Instance.OnDragged -= MovePlayer;
        }

    }

    private void Update()
    {

        if (!idPlayer.soonIDie)
        {
            visualMesh.transform.localRotation = Quaternion.Euler(0, 180, 0);
        }

        Vector3 pos = transform.position;

        pos.z = Mathf.Clamp(transform.position.z, verticalLimits.x,verticalLimits.y);
        pos.x = Mathf.Clamp(transform.position.x, horizontallLimits.x, horizontallLimits.y);

        transform.position = pos;

        if (_agent)
        {
            // aumentar por cada huevo recogido
           //  _agent.speed = bufferSpeed + ((increseSpeedPerBox) ? 0.2f : -0.2f * (idPlayer.gifts.Count - 2)) + speedBonus;

            _agent.speed = speedBonus;

            transform.rotation = Quaternion.LookRotation(_agent.velocity.normalized);

            targei.position = transform.position + directionTarget;
            _agent.SetDestination(targei.position);


        }
        else
        {
            // aumentar por cada huevo recogido
            //  speed = bufferSpeed + ((increseSpeedPerBox) ? 0.2f : -0.2f * (idPlayer.gifts.Count - 2)) + speedBonus;
            speed = speedBonus;

        }

        if (idPlayer.gifts.Count > 2)
        {
            Camera.main.SendMessage("Zoom", idPlayer.gifts[idPlayer.gifts.Count - 2].transform);
            
            idPlayer.textNameOver.gameObject.SetActive(true);

        }


        if (IMoving || inDeploymentZone)
        {
            for (int i = 1; i < idPlayer.gifts.Count; i++)
            {
                currentPos = idPlayer.gifts[i].transform;
                previousPos = idPlayer.gifts[i - 1].transform;

                float dist = Vector3.Distance(previousPos.position, currentPos.position);
                Vector3 newPos = previousPos.position;
                newPos.y = transform.position.y;

                float t = Time.deltaTime * dist / minDistGifts * 6;

                idPlayer.gifts[i].transform.position = Vector3.Slerp(currentPos.position, newPos, t);

            }

        }

        if (Vector3.Distance(transform.position, container.position) < 9 && idPlayer.gifts.Count > 1)
        {
            inDeploymentZone = true;
            timeShot += Time.deltaTime;
            if (timeShot > 0.1f)
            {
                ShotGifts();
                timeShot = 0;
            }

        }
        else
        {
            inDeploymentZone = false;
        }
    }

    
    [SerializeField] Transform targei;
    Vector3 directionTarget;

    void MovePlayer(Vector3 direction)
    {
        IMoving = true;

        if (_agent != null)
        {
            IMoving = true;

            directionTarget = new Vector3(direction.x, 0, direction.y).normalized;
        }
        else
        {
            IMoving = true;

            if (invertMovement)
            {
                transform.rotation = Quaternion.LookRotation(new Vector3(-direction.y, 0, direction.x).normalized);
                GetComponent<Rigidbody>().velocity = transform.forward * speed;

            }
            else
            {
                transform.rotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.y).normalized);
                GetComponent<Rigidbody>().velocity = transform.forward * speed;

            }
        }

    }

    void StopPlayer(Vector3 direction)
    {
        IMoving = false;
        GetComponent<Rigidbody>().velocity = Vector3.zero;

    }

    void ShotGifts()
    {
        OnEmotion?.Invoke(Emotion.Happy);
        Vector3 direction = container.transform.position - transform.position;

        if (idPlayer.gifts.Count > 1 && inDeploymentZone)
        {
            idPlayer.gifts[1].transform.position = transform.position+transform.localScale + transform.up * 1.2f + transform.forward * 1.2f;
            idPlayer.gifts[1].GetComponent<Rigidbody>().AddForce( direction * 4 , ForceMode.Impulse);
            idPlayer.gifts[1].GetComponent<Rigidbody>().useGravity = true;
            idPlayer.gifts[1].GetComponent<Collider>().isTrigger = false;
            idPlayer.gifts[1].GetComponent<Ball>().collected = false;
            idPlayer.gifts[1].GetComponent<Ball>().shoted = true;
            idPlayer.gifts[1].GetComponent<Ball>().SetOwner(null);
            idPlayer.gifts.RemoveAt(1);
        }

    }


    void RestoreCollider()
    {
        GetComponent<Collider>().enabled = true;

    }


    public IEnumerator ImproveVelocity()
    {
        // antes de huevos
       // speedBonus = bufferSpeed;

        speedBonus = bufferSpeed * 1.8f;
        dashParticle.SetActive(true);

        yield return new WaitForSeconds(2f);
       

        LessVelocity();
    }

    public void LessVelocity()
    {

        //antes huevos
        // speedBonus = 0;
        dashParticle.SetActive(false);

        speedBonus = bufferSpeed;

    }

    void Stop()
    {
        _agent.isStopped = true;
        GetComponent<Collider>().enabled = false;
    }

    void DeadAnimation()
    {
        visualMesh.SendMessage("CheckAnimation");
        visualMesh.SendMessage("SetTarget2Position",new Vector3(0,-1,0));
        visualMesh.SendMessage("SetRotation",Vector3.zero);
    }

    void Continue()
    {
        _agent.isStopped = false;
        GetComponent<Collider>().enabled = true;
    }

    void ReviveAnimation()
    {
        visualMesh.SendMessage("CheckAnimation");
        visualMesh.SendMessage("SetTarget2Position", new Vector3(0, 0, 0));
        visualMesh.SendMessage("SetRotation", new Vector3(0,0,180));
    }

}
