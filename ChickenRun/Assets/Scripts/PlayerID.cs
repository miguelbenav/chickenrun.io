﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerID : MonoBehaviour, IPlayer
{
    public string playerName;
    public List<GameObject> gifts = new List<GameObject>();


    public TextMeshProUGUI textNameOver;
    [SerializeField] NameList nameList;
    public System.Action<Emotion> OnEmotion { get; set; }


    Transform lookPoint;
    Vector3 startPosition;
    public Vector3 scaleFactor = new Vector3(0.06f,0.06f,0.06f);
    AgentPlayer agentAI;

    [HideInInspector]
    public bool soonIDie;

    [SerializeField] GameObject failParticles;
    GameObject failParticles2;


    private void Start()
    {
        if (GetComponent<AgentPlayer>())
        {
            agentAI = GetComponent<AgentPlayer>();
                
        }

        startPosition = transform.position;
        lookPoint = Camera.main.transform;

        if (nameList != null)
        {
            playerName = nameList.namesIA[Random.Range(0, 90)];

        }


        //GameObject.FindGameObjectWithTag("GameController").SendMessage("SetNamesBoard");
    }

    private void Update()
    {
        textNameOver.transform.LookAt(lookPoint);

        transform.localScale =Vector3.one + (gifts.Count * scaleFactor);


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("AwayPlayer"))
        {
            if (other.GetComponent<PlayerID>().gifts.Count > gifts.Count)
            {
                soonIDie = true;

                if (gifts.Count > 1)
                {
                    TakeAwayGifts(gifts[1]);
                }
                else
                {
                    Dead();
                }


            }

        }
        else if (other.CompareTag("Ball"))
        {
            Ball objBall = other.GetComponent<Ball>();

            if (!ReferenceEquals(objBall.owner, gameObject) && objBall.weight <= gifts.Count)
            {
                objBall.SendMessage("ActiveParticles");
                gifts.Add(other.gameObject);
                other.transform.position = transform.position + -transform.forward * 2;
                other.GetComponent<Rigidbody>().useGravity = false;
                other.GetComponent<Rigidbody>().velocity = Vector3.zero;
                other.GetComponent<Collider>().isTrigger = true;
                other.SendMessage("SetOwner", gameObject);

                if (agentAI != null)
                {
                    agentAI.target = null;

                }
            }
        }
    }



    void Dead()
    {
        if (tag.Equals("Player"))
        {
            GameObject.FindGameObjectWithTag("GameController").SendMessage("LosePlayer");
        }

        SendMessage("Stop");
        SendMessage("DeadAnimation");

        if (failParticles2 == null)
        {
            failParticles2 = Instantiate(failParticles, transform.position, Quaternion.identity);
        }
        else
        {
            failParticles2.SetActive(true);
            failParticles2.transform.SetParent(null);
            failParticles2.transform.position = transform.position;
        }

        transform.position = startPosition;

        failParticles.SetActive(true);
        //failParticles.transform.position = transform.position;
        Invoke("Revive", 4);
    }

    void Revive()
    {
        SendMessage("Continue");
        SendMessage("ReviveAnimation");
        failParticles.SetActive(false);
        failParticles2.SetActive(false);
        soonIDie = false;
    }

    public void TakeAwayGifts(GameObject gift)
    {
        OnEmotion?.Invoke(Emotion.Angry);
      //  bool removedGift = false;
        int startIndex=1;

        for (int i = 1; i < gifts.Count; i++)
        {
            if (ReferenceEquals(gifts[i], gift))
            {
                startIndex = i;
                break;
            }
        }
        
        while (startIndex != gifts.Count)
        {
            GameObject gami;
            gami = gifts[startIndex];
            gami.GetComponent<Rigidbody>().useGravity = true;
            gami.GetComponent<Collider>().isTrigger = false;
            gami.GetComponent<Ball>().collected = false;
            gami.GetComponent<Ball>().SetOwner(null);
            gifts.Remove(gami);
            

        }

        if (soonIDie)
        {
            Dead();
        }
    }

    public void TakeAwayGift(GameObject gift)
    {
        foreach (GameObject _gifts in gifts)
        {
            if (ReferenceEquals(_gifts, gift))
            {
                gifts.Remove(_gifts);
                break;
            }
        }

    }

    public GameObject GetVisualPlayerObject()
    {

        if (GetComponent<PlayerController>())
        {
            return GetComponent<PlayerController>().visualMesh;

        }
        else
        {
            return GetComponent<AgentPlayer>().visualMesh;

        }

    }

    public void UpdateName()
    {
        if (CompareTag("Player"))
        {
            playerName = PlayerPrefs.GetString("Name");

        }
        textNameOver.text = playerName;
    }

}
