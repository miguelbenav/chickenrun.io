﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(other.GetComponent<PlayerController>().ImproveVelocity());

        }
        else if (other.CompareTag("AwayPlayer"))
        {
            StartCoroutine(other.GetComponent<AgentPlayer>().ImproveVelocity());
        }
    }

}
