﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoringSystem : MonoBehaviour
{

    public int score;
    [SerializeField] TextMeshProUGUI _text;

    public string _namePlayer;

    private void Awake()
    {
        _text = GetComponent<TextMeshProUGUI>();
    }

    public void SetNamePlayerScoringSystem(string newName)
    {
        _namePlayer = newName;
        ShowScore();
    }

    public void ShowScore()
    {
        _text.text = _namePlayer + " " + score;
    }

    public void AddScore()
    {
        score++;
        ShowScore();
    }

    public void AddScore(int plusScore)
    {
        score += plusScore;
        ShowScore();

    }
}
