﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparentObject : MonoBehaviour
{

    [SerializeField] Material transparentMaterial;

    public MeshRenderer[] childsRender;
    Material[] bufferMaterials;



    void Start()
    {
    //    childsRender = new MeshRenderer[transform.childCount];
        bufferMaterials = new Material[childsRender.Length];

        for (int i = 0; i < childsRender.Length; i ++)
        {
          //  print(transform.GetChild(i).name);
        //    childsRender[i] = transform.GetChild(i).GetComponent<MeshRenderer>();
            bufferMaterials[i] = childsRender[i].materials[0];
        }
    }

    private void Update()
    {

        //if (Input.GetKeyDown(KeyCode.H))
        //{

        //    GetComponent<MeshRenderer>().material = transparentMaterial; 
        //}

    }

    void ApplyTransparency()
    {
        foreach (MeshRenderer renderChild in childsRender)
        {
            renderChild.material= transparentMaterial;
                
        }
    }


    void ResetMaterial()
    {

        for (int i =0; i< childsRender.Length; i++)
        {
            childsRender[i].material = bufferMaterials[i];
        }
    }
}
